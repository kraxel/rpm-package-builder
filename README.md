Container images for test builds and rpm package builds.

Master branch has this README.md and some include files.

Each distro (centos7, centos8, fedora) has its own branch.

The include file can be used this way in .gitlab-ci.yml:

```
stages:
  - rpms
  - repo

include:
  - project: 'kraxel/rpm-package-builder'
    file: 'gitlab-ci-rpms-tito.yml'
```
